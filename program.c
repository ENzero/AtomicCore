#include "Core/gpio.h"

#define test 0x80

int main() 
{
	int d;
	Initialise(PORT_B, test, OUTPUT);

	//SetOutput(PORT_B,test,HIGH);
	//PB_ODR = 0x20;
	
	// Loop
	do {
		//SetOutput(PORT_C,test,GetPinValue(PORT_C,test));
		PB_ODR ^= test;
		for(d = 0; d < 30000; d++) { }
	} while(1);
}
