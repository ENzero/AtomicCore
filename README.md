About
==================

This project contains the Ghosted API
specificly for the stm8s103f3p6 board. 

Compatibility
==================
STM8S103F2, STM8S103F3, STM8S103K3

Getting Started
==================
- Install SDCC (Small Devices C Compiler)
	- $ sudo apr-get install sdcc

- Insall stm8flash
	- $ git clone https://github.com/vdudouyt/stm8flash.git
	- $ cd stm8flash
	- $ make
	- $ sudo make install

- Connect stlinkv2 → stm8
	- 3.3V → 3V3
	- SWIM → SWM
	- GND → GND
	- RST → NRST

Compiling
==================
$ make

Flash
==================
$ make flash

Shortcut(compiling and flashing)
==================
./run.sh

