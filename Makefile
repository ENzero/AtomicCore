SDCC=sdcc
SDLD=sdld
CFLAGS = -mstm8 --std-c99
OBJECT=program.ihx

.PHONY: all clean flash

all: $(OBJECT)

clean:
	rm -f *.asm *.ihx *.cdb *.lst *.map *.lk *.rel *.rst *.sym

flash:
	stm8flash -c stlinkv2 -p stm8s103?3 -w $(OBJECT)

%.ihx: %.c
	$(SDCC) $(CFLAGS) $(LDFLAGS) $<
