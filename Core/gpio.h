/*****************************************************************************/
/*											GPIO Handler												*/
/*****************************************************************************/
/*	File Name : gpio.h																				*/
/*	Author : Angelo Charl Sison																*/
/*	Date : 24/11/2017																				*/
/*	Description :This header file contains the API for manipulating the 		*/
/*						GPIO pins of STM8s103.													*/
/*****************************************************************************/

#include "STM8S103.h"

/* GPIO Mode */
#define OUTPUT 1
#define INPUT 0

/* Signal States */
#define HIGH 1
#define LOW 0

/* Port */
#define PORT_A 'a'
#define PORT_B 'b'
#define PORT_C 'c'
#define PORT_D 'd'
#define PORT_E 'e'
#define PORT_F 'f'

/* Pin Numbers */
#define A1 0x02
#define A2 0x04
#define A3 0x08

#define B2 0x20
#define B4 0x40	/* Temporary */
#define B5 0x50	/* Temporary */

#define C3 0x08
#define C4 0x10
#define C5 0x20
#define C6 0x40
#define C7 0x80

#define D1 0x10 /* Temporary */
#define D2 0x04
#define D3 0x08
#define D4 0x10
#define D5 0x20
#define D6 0x50

/*****************************************************************************/
/*	Function : void Initialise(char port, char pinNumber, int pinMode)			*/
/*	Return Value : N/A																				*/
/*	Parameters : 																					*/
/*		- port : The port block.																	*/
/*		- pinNumber : The pin number on the chip.										*/
/*		- pinMode : The desired mode of the pin.											*/
/*	Description : The function initialises GPIO port pins and set the pin		*/
/*						mode. The pinMode is either 	OUTPUT or INPUT.				*/
/*****************************************************************************/
void Initialise(char port, char pinNumber, int pinMode)
{
	switch(port)
	{
		case 'a' | 'A' :
			PA_DDR |= pinMode ? pinNumber : 0xff;
			PA_CR1 |= pinNumber;
			break;
		case 'b' | 'B' :
			PB_DDR |= pinMode ? pinNumber : 0xff;
			PB_CR1 |= pinNumber;
			break;
		case 'c' | 'C' :
			PC_DDR |= pinMode ? pinNumber : 0xff;
			PC_CR1 |= pinNumber;
			break;
		case 'd' | 'D' :
			PD_DDR |= pinMode ? pinNumber : 0xff;
			PD_CR1 |= pinNumber;
			break;
		case 'e' | 'E' :
			PE_DDR |= pinMode ? pinNumber : 0xff;
			PE_CR1 |= pinNumber;
			break;
		case 'f' | 'F' :
			PF_DDR |= pinMode ? pinNumber : 0xff;
			PF_CR1 |= pinNumber;			
			break;
		default :
			/* Nothing to be done. */
			break;
	}
}

/*****************************************************************************/
/*	Function : void SetOutput(char port, char pinNumber, int output)			*/
/*	Return Value : N/A																				*/
/*	Parameters : 																					*/
/*		- port : The port block.																	*/
/*		- pinNumber : The pin number on the chip.										*/
/*		- pinMode : The desired output value of the pin.								*/
/*	Description : The function sets the output value of a pin.						*/
/*****************************************************************************/
void SetOutput(char port, char pinNumber, int output)
{
	switch(port)	
	{
		case 'a' | 'A':
			PA_ODR = output ? (PA_ODR | pinNumber) : (PA_ODR & ~pinNumber);
			break;
		case 'b' | 'B' :
			PB_ODR = output ? (PB_ODR | pinNumber) : (PB_ODR & ~pinNumber);
			break;
		case 'c' | 'C':
			PC_ODR = output ? (PC_ODR | pinNumber) : (PC_ODR & ~pinNumber);
			break;
		case 'd' | 'D' :
			PD_ODR = output ? (PD_ODR | pinNumber) : (PD_ODR & ~pinNumber);
			break;
		case 'e' | 'E' :
			PE_ODR = output ? (PE_ODR | pinNumber) : (PE_ODR & ~pinNumber);
			break;
		case 'f' | 'F' :
			PF_ODR = output ? (PF_ODR | pinNumber) : (PF_ODR & ~pinNumber);
			break;
		default :
			/* Nothing to be done. */
			break;
	}
}

/*****************************************************************************/
/*	Function : int GetPinValue(char port, char pinNumber)							*/
/*	Return Value : The current value of the pin's output.							*/
/*	Parameters : 																					*/
/*		- port : The port block.																	*/
/*		- pinNumber : The pin number on the chip.										*/
/*	Description : The function gets the current value of the pin's output.	*/
/*****************************************************************************/
int GetPinValue(char port, char pinNumber)
{
		switch(port)
	{
		case 'a' | 'A' :
			return ((PA_ODR & pinNumber) != 0);
		case 'b' | 'B' :
			return ((PB_ODR & pinNumber) != 0);
		case 'c' | 'C' :
			return ((PC_ODR & pinNumber) != 0);
		case 'd' | 'D' :
			return ((PD_ODR & pinNumber) != 0);
		case 'e' | 'E' :
			return ((PE_ODR & pinNumber) != 0);
		case 'f' | 'F' :
			return ((PF_ODR & pinNumber) != 0);
		default :
			/* Nothing to be done. */
			return 0;
	}
}






