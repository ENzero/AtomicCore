/*****************************************************************************/
/*											Register Declaration									*/
/*****************************************************************************/
/*	File Name : STM8S103.h																		*/
/*	Author : Angelo Charl Sison																*/
/*	Date : 24/11/2017																				*/
/*	Description :The Following addresses are based from ST's datasheet 	*/
/*						for stm8s103f3p6. Here is the link to the datasheet :		*/
/*	http://www.st.com/content/ccc/resource/technical/document/datasheet/ce/13/13/03/a9/a4/42/8f/CD00226640.pdf/files/CD00226640.pdf/jcr:content/translations/en.CD00226640.pdf	*/
/*****************************************************************************/

/* GPIO */
#define PA_ODR *(unsigned char*)0x5000 	/* Port A data output latch register */
#define PA_IDR *(unsigned char*)0x5001 		/* Port A input pin value register  */
#define PA_DDR *(unsigned char*)0x5002	/* Port A data direction register  */
#define PA_CR1 *(unsigned char*)0x5003 	/* Port A control register 1 */
#define PA_CR2 *(unsigned char*)0x5004 	/* Port A control register 2 */

#define PB_ODR *(unsigned char*)0x5005 	/* Port B data output latch register */
#define PB_IDR *(unsigned char*)0x5006 		/* Port B input pin value register */
#define PB_DDR *(unsigned char*)0x5007 	/* Port B data direction register */
#define PB_CR1 *(unsigned char*)0x5008 	/* Port B control register 1 */
#define PB_CR2 *(unsigned char*)0x5009 	/* Port B control register 2 */

#define PC_ODR *(unsigned char*)0x500A	/* Port C data output latch register */
#define PC_IDR *(unsigned char*)0x500B		/* Port C input pin value register */
#define PC_DDR *(unsigned char*)0x500C	/* Port C data direction register */
#define PC_CR1 *(unsigned char*)0x500D	/* Port C control register 1 */
#define PC_CR2 *(unsigned char*)0x500E		/* Port C control register 2 */

#define PD_ODR *(unsigned char*)0x500F	/* Port D data output latch register */
#define PD_IDR *(unsigned char*)0x5010		/* Port D input pin value register */
#define PD_DDR *(unsigned char*)0x5011	/* Port D data direction register */
#define PD_CR1 *(unsigned char*)0x5012		/* Port D control register 1 */
#define PD_CR2 *(unsigned char*)0x5013		/* Port D control register 2 */

#define PE_ODR *(unsigned char*)0x5014	/* Port E data output latch register */
#define PE_IDR *(unsigned char*)0x5015		/* Port E input pin value register */
#define PE_DDR *(unsigned char*)0x5016	/* Port E data direction register */
#define PE_CR1 *(unsigned char*)0x5017		/* Port E control register 1 */
#define PE_CR2 *(unsigned char*)0x5018		/* Port E control register 2 */

#define PF_ODR *(unsigned char*)0x5019	/* Port F data output latch register */
#define PF_IDR *(unsigned char*)0x501A		/* Port F input pin value register */
#define PF_DDR *(unsigned char*)0x501B	/* Port F data direction register */
#define PF_CR1 *(unsigned char*)0x501C		/* Port F control register 1 */
#define PF_CR2 *(unsigned char*)0x501D		/* Port F control register 2 */

/* FLASH */
//#define FLASH_CR1 *(unsigned char*)0x505A		/* Flash control register 1 */
//#define FLASH_CR2 *(unsigned char*)0x505B		/* Flash control register 2 */
//#define FLASH_NCR2 *(unsigned char*)0x505C		/* Flash complementary control register 2 */
//#define FLASH _FPR *(unsigned char*)0x505D		/* Flash protection register */
//#define FLASH _NFPR *(unsigned char*)0x505E		/* Flash complementary protection register */
//#define FLASH _IAPSR *(unsigned char*)0x505F		/* Flash in-application programming status register */
//#define FLASH _PUKR *(unsigned char*)0x5062		/* Flash program memory unprotection register */
//#define  FLASH _DUKR *(unsigned char*)0x5064	/* Data EEPROM unprotection register */

/* EXTERNAL INTERRUPT */
#define EXTI_CR1	*(unsigned char*)0x50A0	/* External interrupt control register 1 */
#define EXTI_CR2	*(unsigned char*)0x50A1	/* External interrupt control register 2 */

/* RESET */
#define RST_SR	*(unsigned char*)0x50B3		/* Reset status register */

/* CLOCK */
#define CLK_ICKR	*(unsigned char*)0x50C0			/* Internal clock control register */
#define CLK_ECKR	*(unsigned char*)0x50C1			/* External clock control register */
#define CLK_CMSR	*(unsigned char*)0x50C3			/* Clock master status register */
#define CLK_SWR	*(unsigned char*)0x50C4			/* Clock master switch register */
#define CLK_SWCR	*(unsigned char*)0x50C5			/* Clock switch control register */
#define CLK_CKDIVR	*(unsigned char*)0x50C6		/* Clock divider register */
#define CLK_PCKENR1	*(unsigned char*)0x50C7	/* Peripheral clock gating register 1 */
#define CLK_CSSR 	*(unsigned char*)0x50C8			/* Clock security system register */	
#define CLK_CCOR	*(unsigned char*)0x50C9			/* Configurable clock control register */
#define CLK_PCKENR2	*(unsigned char*)0x50CA	/* Peripheral clock gating register 2 */
#define CLK_HSITRIMR	*(unsigned char*)0x50CC	/* HSI clock calibration trimming register */
#define CLK_SWIMCCR	*(unsigned char*)0x50CD	/* SWIM clock control register */

/* WWDG */
#define WWDG_CR *(unsigned char*)0x50D1		/*  WWDG control register */
#define WWDG_WR *(unsigned char*)0x50D2		/* WWDR window register */

/* IWDG */
#define IWDG_KR *(unsigned char*)0x50E0		/* IWDG key register */
#define IWDG_PR *(unsigned char*)0x50E1		/* IWDG prescaler register */
#define IWDG_RLR *(unsigned char*)0x50E2	/* IWDG reload register */

/* AWU */
#define AWU_CSR1 *(unsigned char*)0x5230	/* AWU control/status register 1 */
#define AWU_APR *(unsigned char*)0x5231		/* AWU asynchronous prescaler buffer register */	
#define AWU_TBR *(unsigned char*)0x5232		/* AWU timebase selection register */

/* BEEP */
#define BEEP_CSR *(unsigned char*)0x50F3		/* BEEP control/status register */

/* SPI */
#define SPI_CR1 *(unsigned char*)0x5200		/*  SPI control register 1 */
#define SPI_CR2 *(unsigned char*)0x5201		/*  SPI control register 2 */	
#define SPI_ICR *(unsigned char*)0x5202			/* SPI interrupt control register */
#define SPI_SR *(unsigned char*)0x5203			/* SPI status register */
#define SPI_DR *(unsigned char*)0x5204			/* SPI data register */	
#define SPI_CRCPR *(unsigned char*)0x5205	/* SPI CRC polynomial register */
#define SPI_RXCRCR *(unsigned char*)0x5206	/* SPI Rx CRC register */
#define SPI_TXCRCR *(unsigned char*)0x5207	/* SPI Tx CRC register */

/* I2C */
#define I2C_CR1 *(unsigned char*)0x5210		/* I2C control register 1 */
#define I2C_CR2 *(unsigned char*)0x5211		/* I2C control register 2 */	
#define I2C_FREQR *(unsigned char*)0x5212	/* I2C frequency register */
#define I2C_OARL *(unsigned char*)0x5213		/* I2C Own address register low */
#define I2C_OARH *(unsigned char*)0x5214		/* I2C Own address register high */
#define I2C_DR *(unsigned char*)0x5216			/* I2C data register */
#define I2C_SR1 *(unsigned char*)0x5217		/* I2C status register 1 */
#define I2C_SR2 *(unsigned char*)0x5218		/* I2C status register 2 */
#define I2C_SR3 *(unsigned char*)0x5219		/* I2C status register 3 */
#define I2C_ITR *(unsigned char*)0x521A			/* I2C interrupt control register */
#define I2C_CCRL *(unsigned char*)0x521B		/* I2C Clock control register low */
#define I2C_CCRH *(unsigned char*)0x521C		/* I2C Clock control register high */
#define I2C_TRISER *(unsigned char*)0x521D	/* I2C TRISE register */	
#define I2C_PECR *(unsigned char*)0x521E		/* I2C packet error checking register */

/* UART */
#define UART1_SR *(unsigned char*)0x5230			/* UART1 status register */
#define UART1_DR *(unsigned char*)0x5231		/* UART1 data register */	
#define UART1_BRR1 *(unsigned char*)0x5232	/* UART1 baud rate register 1 */
#define UART1_BRR2 *(unsigned char*)0x5233	/* UART1 baud rate register 2 */
#define UART1_CR1 *(unsigned char*)0x5234		/*  UART1 control register 1 */
#define UART1_CR2 *(unsigned char*)0x5235		/* UART1 control register 2 */
#define UART1_CR3 *(unsigned char*)0x5236		/*  UART1 control register 3 */
#define UART1_CR4 *(unsigned char*)0x5237		/* UART1 control register 4 */
#define UART1_CR5 *(unsigned char*)0x5238		/* UART1 control register 5 */
#define UART1_GTR *(unsigned char*)0x5239		/* UART1 guard time register */
#define UART1_PSCR *(unsigned char*)0x523A	/* UART1 prescaler register */

/* ------------------- USART ------------------- */
#define USART1_SR *(unsigned char*)0x5230		/* aa */
#define USART1_DR *(unsigned char*)0x5231		/* aa */	
#define USART1_BRR1 *(unsigned char*)0x5232	/* aa */
#define USART1_BRR2 *(unsigned char*)0x5233	/* aa */
#define USART1_CR1 *(unsigned char*)0x5234		/* aa */
#define USART1_CR2 *(unsigned char*)0x5235		/* aa */
#define USART1_CR3 *(unsigned char*)0x5236		/* aa */
#define USART1_CR4 *(unsigned char*)0x5237		/* aa */
#define USART1_CR5 *(unsigned char*)0x5238		/* aa */
#define USART1_GTR *(unsigned char*)0x5239	/* aa */
#define USART1_PSCR *(unsigned char*)0x523A	/* aa */

/* USART_CR1 bits */
#define USART_CR1_R8 (1 << 7)
#define USART_CR1_T8 (1 << 6)
#define USART_CR1_UARTD (1 << 5)
#define USART_CR1_M (1 << 4)
#define USART_CR1_WAKE (1 << 3)
#define USART_CR1_PCEN (1 << 2)
#define USART_CR1_PS (1 << 1)
#define USART_CR1_PIEN (1 << 0)

/* USART_CR2 bits */
#define USART_CR2_TIEN (1 << 7)
#define USART_CR2_TCIEN (1 << 6)
#define USART_CR2_RIEN (1 << 5)
#define USART_CR2_ILIEN (1 << 4)
#define USART_CR2_TEN (1 << 3)
#define USART_CR2_REN (1 << 2)
#define USART_CR2_RWU (1 << 1)
#define USART_CR2_SBK (1 << 0)

/* USART_CR3 bits */
#define USART_CR3_LINEN (1 << 6)
#define USART_CR3_STOP2 (1 << 5)
#define USART_CR3_STOP1 (1 << 4)
#define USART_CR3_CLKEN (1 << 3)
#define USART_CR3_CPOL (1 << 2)
#define USART_CR3_CPHA (1 << 1)
#define USART_CR3_LBCL (1 << 0)

/* USART_SR bits */
#define USART_SR_TXE (1 << 7)
#define USART_SR_TC (1 << 6)
#define USART_SR_RXNE (1 << 5)
#define USART_SR_IDLE (1 << 4)
#define USART_SR_OR (1 << 3)
#define USART_SR_NF (1 << 2)
#define USART_SR_FE (1 << 1)
#define USART_SR_PE (1 << 0)


/* ------------------- TIMERS ------------------- */
#define TIM1_CR1 *(unsigned char*)0x52B0
#define TIM1_CR2 *(unsigned char*)0x52B1
#define TIM1_SMCR *(unsigned char*)0x52B2
#define TIM1_ETR *(unsigned char*)0x52B3
#define TIM1_DER *(unsigned char*)0x52B4
#define TIM1_IER *(unsigned char*)0x52B5
#define TIM1_SR1 *(unsigned char*)0x52B6
#define TIM1_SR2 *(unsigned char*)0x52B7
#define TIM1_EGR *(unsigned char*)0x52B8
#define TIM1_CCMR1 *(unsigned char*)0x52B9
#define TIM1_CCMR2 *(unsigned char*)0x52BA
#define TIM1_CCMR3 *(unsigned char*)0x52BB
#define TIM1_CCMR4 *(unsigned char*)0x52BC
#define TIM1_CCER1 *(unsigned char*)0x52BD
#define TIM1_CCER2 *(unsigned char*)0x52BE
#define TIM1_CNTRH *(unsigned char*)0x52BF
#define TIM1_CNTRL *(unsigned char*)0x52C0
#define TIM1_PSCRH *(unsigned char*)0x52C1
#define TIM1_PSCRL *(unsigned char*)0x52C2
#define TIM1_ARRH *(unsigned char*)0x52C3
#define TIM1_ARRL *(unsigned char*)0x52C4
#define TIM1_RCR *(unsigned char*)0x52C5
#define TIM1_CCR1H *(unsigned char*)0x52C6
#define TIM1_CCR1L *(unsigned char*)0x52C7
#define TIM1_CCR2H *(unsigned char*)0x52C8
#define TIM1_CCR2L *(unsigned char*)0x52C9
#define TIM1_CCR3H *(unsigned char*)0x52CA
#define TIM1_CCR3L *(unsigned char*)0x52CB
#define TIM1_CCR4H *(unsigned char*)0x52CC
#define TIM1_CCR4L *(unsigned char*)0x52CD
#define TIM1_BKR *(unsigned char*)0x52CE
#define TIM1_DTR *(unsigned char*)0x52CF
#define TIM1_OISR *(unsigned char*)0x52D0
#define TIM1_DCR1 *(unsigned char*)0x52D1
#define TIM1_DCR2 *(unsigned char*)0x52D2
#define TIM1_DMA1R *(unsigned char*)0x52D3

/* TIM_IER bits */
#define TIM_IER_BIE (1 << 7)
#define TIM_IER_TIE (1 << 6)
#define TIM_IER_COMIE (1 << 5)
#define TIM_IER_CC4IE (1 << 4)
#define TIM_IER_CC3IE (1 << 3)
#define TIM_IER_CC2IE (1 << 2)
#define TIM_IER_CC1IE (1 << 1)
#define TIM_IER_UIE (1 << 0)

/* TIM_CR1 bits */
#define TIM_CR1_APRE (1 << 7)
#define TIM_CR1_CMSH (1 << 6)
#define TIM_CR1_CMSL (1 << 5)
#define TIM_CR1_DIR (1 << 4)
#define TIM_CR1_OPM (1 << 3)
#define TIM_CR1_URS (1 << 2)
#define TIM_CR1_UDIS (1 << 1)
#define TIM_CR1_CEN (1 << 0)

/* TIM_SR1 bits */
#define TIM_SR1_BIF (1 << 7)
#define TIM_SR1_TIF (1 << 6)
#define TIM_SR1_COMIF (1 << 5)
#define TIM_SR1_CC4IF (1 << 4)
#define TIM_SR1_CC3IF (1 << 3)
#define TIM_SR1_CC2IF (1 << 2)
#define TIM_SR1_CC1IF (1 << 1)
#define TIM_SR1_UIF (1 << 0)
